package com.devcamp.task56c10.Controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task56c10.models.Region;
import com.devcamp.task56c10.service.RegionService;

@RestController
@CrossOrigin
public class RegionController {
    @Autowired
    private RegionService regionService;
    @GetMapping("/region-info")

    public Region getRegionInfo(@RequestParam(required = true, name = "codeRegion") String regionCode){
         Region allRegion = regionService.fillterAllRegion(regionCode);

         return allRegion;
    }
}
