package com.devcamp.task56c10.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task56c10.models.Country;
import com.devcamp.task56c10.service.CountryService;

@RestController
@CrossOrigin
public class CountryController {
    @Autowired
    private CountryService countryService;

    @GetMapping("/country")
    public ArrayList<Country> getAllCountry(){
        ArrayList<Country> allCountry = countryService.getAllCountry();
        return allCountry;
    }

    @GetMapping("/country-info")
    public Country getCountInfo(@RequestParam(required = true, name = "code") String countryCode){
         ArrayList<Country> allCountry = countryService.getAllCountry();

         Country findCountry = new Country();

         for (Country countryElement : allCountry) {
            if(countryElement.getCountryCode().equals(countryCode)){
                findCountry = countryElement;
            }
         }

         return findCountry;
    }

    
}
