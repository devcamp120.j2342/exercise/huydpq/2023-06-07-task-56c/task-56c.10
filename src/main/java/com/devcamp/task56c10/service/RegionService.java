package com.devcamp.task56c10.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.task56c10.models.Region;

@Service
public class RegionService {
    Region haNoi = new Region("HN", "Hà nội");
    Region hoChiMinh = new Region("HCM", "Hồ Chí Minh");
    Region daNang = new Region("ĐN", "Đà nẵng");

    Region newYork = new Region("NY", "New York");
    Region florida = new Region("FLO", "Florida");
    Region texas = new Region("TX", "Texas");

    Region moscow = new Region("MC", "Moscow");
    Region kaluga = new Region("KL", "Kaluga");
    Region paris = new Region("PR", "Paris");

    public ArrayList<Region> getRegionVN(){
        ArrayList<Region> regionVN = new ArrayList<>();

        regionVN.add(haNoi);
        regionVN.add(hoChiMinh);
        regionVN.add(daNang);
        return regionVN;
    }

    public ArrayList<Region> getRegionUS(){
        ArrayList<Region> regionUS = new ArrayList<>();

        regionUS.add(newYork);
        regionUS.add(florida);
        regionUS.add(texas);
        return regionUS;
    }

    public ArrayList<Region> getRegionRussia(){
        ArrayList<Region> regionRussia = new ArrayList<>();

        regionRussia.add(moscow);
        regionRussia.add(kaluga);
        regionRussia.add(paris);
        return regionRussia;
    }

    public Region fillterAllRegion(String regionCode){
        ArrayList<Region> allRegion = new ArrayList<>();

         allRegion.add(haNoi);
        allRegion.add(hoChiMinh);
        allRegion.add(daNang);
        allRegion.add(newYork);
        allRegion.add(florida);
        allRegion.add(texas);
        allRegion.add(moscow);
        allRegion.add(kaluga);
        allRegion.add(paris);

        Region region = new Region();
        for (Region regionElement : allRegion) {
            if(regionElement.getRegionCode().equals(regionCode)){
                region = regionElement;
            }
        }
        return region;
    }

}
