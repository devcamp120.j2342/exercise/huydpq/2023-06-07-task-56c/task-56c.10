package com.devcamp.task56c10.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.task56c10.models.Country;

@Service
public class CountryService {
    @Autowired

    private RegionService regionService;

    Country vietname = new Country("VN","Việt Name");
    Country us = new Country("US","Mỹ");
    Country russia = new Country("Russia","Nga");

    public ArrayList<Country> getAllCountry() {
        vietname.setRegion(regionService.getRegionVN());
        us.setRegion(regionService.getRegionUS());
        russia.setRegion(regionService.getRegionRussia());
        //-------------------------
        ArrayList <Country> countries = new ArrayList<>();
        countries.add(vietname);
        countries.add(us);
        countries.add(russia);

        return countries;
    }

}
