package com.devcamp.task56c10;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task56C10Application {

	public static void main(String[] args) {
		SpringApplication.run(Task56C10Application.class, args);
	}

}
